package myBridge;

public abstract class Shape {
	// define
    protected Draw draw;
    // construct
    protected Shape(Draw draw) {
    	this.draw = draw;
    }
    // draw
    public abstract void drawMe();
}
