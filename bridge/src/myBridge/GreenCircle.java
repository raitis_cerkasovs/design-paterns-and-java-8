package myBridge;

public class GreenCircle implements Draw {
	@Override
	public void drawCircle(int radius, int x, int y) {
        System.out.println("Green circle: " + x + " " + y + " " + radius);				
	}
}
