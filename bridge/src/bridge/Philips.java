package bridge;

public class Philips implements TV {

	@Override
	public void on() {
		System.out.println("Philips is on");

	}

	@Override
	public void off() {
		System.out.println("Philips is off");

	}

	@Override
	public void tuneChannel(int channel) {
		System.out.println("Philips tune channel: " + channel);

	}

}
