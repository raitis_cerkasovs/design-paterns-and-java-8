package bridge;

public interface TV {

	public abstract void on();

	public abstract void off();

	public abstract void tuneChannel(int channel);

}