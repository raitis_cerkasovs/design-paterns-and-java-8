package bridge;

public class ConcreteRemote extends RemoteControl {
	
	private int currentChannel;
	
	public void nextChannel() {
		currentChannel++;
		setChannel(currentChannel);
	}
	
	public void prevChannel() {
		currentChannel--;
		setChannel(currentChannel);
	}
	
	public static void main(String[] args) {
		
		ConcreteRemote cr = new ConcreteRemote();
		
		cr.setImplementor(new Philips());
		cr.on();
		cr.setChannel(3);
		cr.prevChannel();

	}

}
