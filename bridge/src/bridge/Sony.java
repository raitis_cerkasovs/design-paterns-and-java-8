package bridge;

public class Sony implements TV {
	
	/* (non-Javadoc)
	 * @see bridge.TV#on()
	 */
	@Override
	public void on() {
		System.out.println("Sony is on");
	}
	
	/* (non-Javadoc)
	 * @see bridge.TV#off()
	 */
	@Override
	public void off() {
		System.out.println("Sony is off");
	}	
	
	/* (non-Javadoc)
	 * @see bridge.TV#tuneChannel(int)
	 */
	@Override
	public void tuneChannel(int channel) {
		System.out.println("Sony tune channel: " + channel );
	}

}
