
public class AE_AccessLocalVariables {

	public static void main(String[] args) {
		
	//dont have to be final
	//	final int num = 1;
	    int num = 1;
		
		Converter<Integer, String> stringConverter =
		        (from) -> String.valueOf(from + num);

		System.out.println(stringConverter.convert(2)); 
	
	// will not compile
	//	num=4;
	}

}
