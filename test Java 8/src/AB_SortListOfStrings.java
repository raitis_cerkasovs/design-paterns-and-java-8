import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public class AB_SortListOfStrings {


	
	
	public static void main(String[] args) {

		List<String> names = Arrays.asList("peter", "anna", "mike", "xenia");	
		
// old way		
//		Collections.sort(names, new Comparator<String>() {
//		    @Override
//		    public int compare(String a, String b) {
//		        return b.compareTo(a);
//		    }
//		});
		
	    
// lambda way		
		Collections.sort(names, (String a, String b) -> {
		    return b.compareTo(a);
		});
		
		System.out.println(names);

	}

}
