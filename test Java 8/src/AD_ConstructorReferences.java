
public class AD_ConstructorReferences {
	
	public static void main(String[] args) {
		
// access static function		
		Something something = new Something();
		Converter<String, String> converter = something::startsWith;
		String converted = converter.convert("Java");
		System.out.println(converted);    // "J"
		
// reference to constructor
		PersonFactory<Person> personFactory = Person::new;
		Person person = personFactory.create("Peter", "Parker");
		System.out.println(person);
	}

}

@FunctionalInterface
interface Converter<F, T> {
	    T convert(F from);
	}

class Something {
    String startsWith(String s) {
        return String.valueOf(s.charAt(0));
    }
}

////////////////////////////
class Person {
    String firstName;
    String lastName;

    Person() {}

    Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

	@Override
	public String toString() {
		return "Person [firstName=" + firstName + ", lastName=" + lastName
				+ "]";
	}
    
    
}


interface PersonFactory<P extends Person> {
    P create(String firstName, String lastName);
}