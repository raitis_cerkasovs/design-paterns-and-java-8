import java.util.Comparator;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;


public class AE_BuiltFunctionalInterfaces {
	
public static void main(String[] args) {

// predicate
	Predicate<String> predicate = (s) -> s.length() > 0;

	predicate.test("foo");              // true
	predicate.negate().test("foo");     // false

	Predicate<Boolean> nonNull = Objects::nonNull;
	Predicate<Boolean> isNull = Objects::isNull;

	Predicate<String> isEmpty = String::isEmpty;
	Predicate<String> isNotEmpty = isEmpty.negate();
	
	System.out.println(nonNull);
	System.out.println(isNull);
	System.out.println(isNotEmpty);
	System.out.println(isEmpty);
	
// functions
	Function<String, Integer> toInteger = Integer::valueOf;
	Function<String, String> backToString = toInteger.andThen(String::valueOf);

	System.out.println(backToString.apply("123"));  
	
	
// comparators
	Comparator<Person> comparator = (p1, p2) -> p1.firstName.compareTo(p2.firstName);

	Person p1 = new Person("John", "Doe");
	Person p2 = new Person("Alice", "Wonderland");

	System.out.println(comparator.compare(p1, p2));             // > 0
	System.out.println(comparator.reversed().compare(p1, p2));  // < 0
}
}
