
public class AA_Runer {
    
    
    public static void main(String[] args) {
    	
    	AA_DefaultInterfaces formula = new AA_DefaultInterfaces() {
    		
    	    @Override
    	    public double calculate(int a) {
    	        return sqrt(a * 100);
    	    }
    	};

    	System.out.println(formula.calculate(100)); 
    	System.out.println(formula.sqrt(16));  
	}
    
}
