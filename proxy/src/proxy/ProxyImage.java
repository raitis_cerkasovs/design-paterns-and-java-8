package proxy;

public class ProxyImage implements Image {
	
	private RealImage image = null;
    private String filename = null;

	public ProxyImage(String filename) {
		super();
		this.filename = filename;
	}

	@Override
	public void displayImage() {
		 if (image == null) {
	           image = new RealImage(filename);
	        } 
	        image.displayImage();
	}
}
