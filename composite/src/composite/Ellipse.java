package composite;

public class Ellipse implements Graphic {
	private String name;
	public Ellipse(String name) {
		super();
		this.name = name;
	}
	@Override
	public void print() {
      System.out.println(name);
	}

}
