package main;

public class HarrierFactory implements AirPlaneFactory {

	/* (non-Javadoc)
	 * @see main.AirPlaneFactory#createFlying()
	 */
	@Override
	public Flying createFlying() {
		FlyingFactory flyingFactory = new FlyingFactory(); 
		return flyingFactory.createFlying("Fighter Jet");
	}

	/* (non-Javadoc)
	 * @see main.AirPlaneFactory#createLiftOff()
	 */
	@Override
	public LiftOff createLiftOff() {
		LiftOffFactory LiftOffFactory = new LiftOffFactory(); 
		return LiftOffFactory.createLiftOff("Verically");
	}

}
