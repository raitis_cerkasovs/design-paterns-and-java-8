package main;

public interface AirPlaneFactory {

	public abstract Flying createFlying();

	public abstract LiftOff createLiftOff();

}