package test;

import static org.junit.Assert.*;
import main.AirPlaneFactory;
import main.Airplane;
import main.Flying;
import main.FlyingFactory;
import main.HarrierFactory;
import main.LiftOff;
import main.LiftOffFactory;

import org.junit.Test;

public class AirplaneTest {

	private FlyingFactory flyingFactory = new FlyingFactory();
	private LiftOffFactory liftOffFactory = new LiftOffFactory();
	private AirPlaneFactory harrierFactory = new HarrierFactory();

	@Test
	public void testFlying_factory() {
		String expectedOutput = "Like fighter Jet"; 
		String stringReturned = null;
		
		Flying fly = flyingFactory.createFlying("Fighter Jet");
		LiftOff liftOff = liftOffFactory.createLiftOff("Verticaly");
		
		Airplane classUnderTest = new Airplane(liftOff, fly);
		
        stringReturned = classUnderTest.howDoYouFly(); 

        assertEquals("Wrong Answer!", expectedOutput, stringReturned);	
     
	}
	
	
	@Test
	public void testFlying_abstractFactory() {
		String expectedOutput = "Like fighter Jet"; 
		String stringReturned = null;
		
		Flying fly = harrierFactory.createFlying();
		LiftOff liftOff = harrierFactory.createLiftOff();
		
		Airplane classUnderTest = new Airplane(liftOff, fly);
		
        stringReturned = classUnderTest.howDoYouFly(); 

        assertEquals("Wrong Answer!", expectedOutput, stringReturned);	
     
	}
	

	
	
}
