package tests;

import static org.junit.Assert.*;
import newCode.AdaptDel;
import oldCode.Person;

import org.junit.Test;

public class AdaptDelTest {

	@Test
	public void test() {
		
		String expectedAnswer = "A person"; 
		String actualAnswer;
		
		AdaptDel classUnderTest = new AdaptDel(new Person());
		
		actualAnswer = classUnderTest.youWho();
		
		assertEquals("Wrong Answer!", expectedAnswer, actualAnswer);	
		
	}

}