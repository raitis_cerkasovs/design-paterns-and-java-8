package tests;

import static org.junit.Assert.*;
import newCode.AdaptInh;

import org.junit.Test;

public class AdaptInhTest {

	@Test
	public void test() {
		String expectedAnswer = "A person";
	    String actualAnswer;
	    
	    AdaptInh classUnderTest = new AdaptInh();
		
	    actualAnswer = classUnderTest.youWho();
		
	    assertEquals("Wrong Answer!", expectedAnswer, actualAnswer);	
	    
	}

}
