package tests;

import static org.junit.Assert.*;
import oldCode.Nesrop;
import oldCode.NesropInt;

import org.junit.Test;

public class NesropTest {

	@Test
	public void test() {
		String expectedAnswer = "Nesrop A";
	    String actualAnswer;
	    
	    NesropInt classUnderTest = new Nesrop();
		
		actualAnswer = classUnderTest.youWho();
		
		assertEquals("Wrong Answer!", expectedAnswer, actualAnswer);
		
	}

}
