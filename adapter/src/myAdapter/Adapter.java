package myAdapter;

public class Adapter implements MediaPlayer {
	
	// interface variable
    AdvancedMediaPlayer adMedPlay;
    
    // constructor
    Adapter(String audioType) {
    	if (audioType.equalsIgnoreCase("vcl"))
    		adMedPlay = new VlcPlayer();    	
    	if (audioType.equalsIgnoreCase("mp4"))
        	adMedPlay = new Mp4Player();
    }

	@Override
	public void play(String audioType, String fileName) {
		
    	if (audioType.equalsIgnoreCase("vcl"))
    		adMedPlay.playVlc(fileName);    	
    	if (audioType.equalsIgnoreCase("mp4"))
    		adMedPlay.playMp4(fileName);    	
	}

}
