package myAdapter;

public class ConcreteMediaPlayer implements MediaPlayer {
	
	// Adapter
	Adapter adapter;

	@Override
	public void play(String audioType, String fileName) {
		
    	if (audioType.equalsIgnoreCase("mp3"))
    		System.out.println("Playing mp3 " + fileName);  	

        // important one
    	else if (audioType.equalsIgnoreCase("vcl") || audioType.equalsIgnoreCase("mp4")) {
            adapter = new Adapter(audioType);
            adapter.play(audioType, fileName);
    	}
    	
    	else System.out.println("Unsuported format.");
	
	}
}
