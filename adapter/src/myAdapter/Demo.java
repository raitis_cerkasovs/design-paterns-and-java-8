package myAdapter;

public class Demo {
	
	public static void main(String[] args) {
		
		ConcreteMediaPlayer concreteMediaPlayer = new ConcreteMediaPlayer();
		
		concreteMediaPlayer.play("mp3", "lalala");
		concreteMediaPlayer.play("xxx", "yyy");
		concreteMediaPlayer.play("VCL", "vcl file");
		concreteMediaPlayer.play("mp4", "mp4 file");
	}
	
}
