package newCode;

import oldCode.NesropInt;
import oldCode.Person;

public class AdaptDel implements NesropInt {

	private Person person;

	public AdaptDel(Person person2) {
        person = person2;
	}

	@Override
	public String youWho() {
		return person.whoAreYou();
	}

}