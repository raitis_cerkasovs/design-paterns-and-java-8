package decorator;

public class EmailSender {
   
   public static void main(String[] args) {
	 
	   
	     ExternalEmailDecorator external = new ExternalEmailDecorator(new Email("external email"));
	     System.out.println(external.getContents()); 
	     
		 SecureEmailDecorator secure = new SecureEmailDecorator(new Email("secure email"));
	     System.out.println(secure.getContents()); 

	
}
   
   
}
