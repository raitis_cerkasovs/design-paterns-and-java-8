package myDecorator;

public abstract class ShapeDecorator implements Shape {

	// interface
	protected Shape decoratedShape;
	
	// construct
	public ShapeDecorator(Shape decoratedShape) {
		this.decoratedShape = decoratedShape;
	}
	
	// impl. method
	public void draw() {
		decoratedShape.draw();
	}
	
}
