package myDecorator;

public class ConcreteDecorator extends ShapeDecorator {

	public ConcreteDecorator(Shape decoratedShape) {
		super(decoratedShape);
	}
	
	public void draw() {
		decoratedShape.draw();
		addMargin();
		
	}
	
	private void addMargin() {
		System.out.println("Red margin");
	}
}
