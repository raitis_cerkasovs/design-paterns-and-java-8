package myDecorator;

public class Demo {

	public static void main(String[] args) {

        Shape circle = new Circle();
        Shape rectangle = new Rectangle();
        
        circle.draw();
        rectangle.draw();
        
        System.out.println("--------------- DECORATED --------------------");
        Shape decoratedCircle = new ConcreteDecorator(new Circle());
        decoratedCircle.draw();
	}

}
