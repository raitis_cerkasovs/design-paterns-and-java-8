package myDecorator;

public interface Shape {
    public void draw();
}
