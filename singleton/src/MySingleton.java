
public class MySingleton {

	
	private MySingleton() {}
	private static MySingleton instance;
	
	public String myTestVar = "initial value";
	
	// can (have) to be synchronized for multi-threading
	public static MySingleton getInstance() {
		if (instance == null) instance =  new MySingleton();
	  return instance;
	}
	
    public void printVar() {
    	System.out.println(myTestVar);
    }
}
