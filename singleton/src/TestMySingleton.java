
public class TestMySingleton {

	public static MySingleton test;
	public static MySingleton test2;

	public static void main(String[] args) {
        
		// 1. cannot do
		// MySingleton test = new MySingleton();
		
		// 2. print initial value 
		MySingleton.getInstance().printVar();
		
		// 3. Initialize
		test = MySingleton.getInstance();
		test.printVar();
		
		// 4. change it
		test.myTestVar = "abracadabra";
		test.printVar();
		
		// 5. same variable
		test2 = test.getInstance();
		test2.printVar();
		

	}

}
