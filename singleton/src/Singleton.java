

import java.util.Random;

public class Singleton {
	
    private static Singleton s_object;
    
    private double r = 1;
    private double random = new Random().nextDouble();
	
    // private constructor prevents to instantiate class
	private Singleton () {
		// can be nothing
		this.r = this.r * this.random; }
	
	public double getR() {
		return r;
	}

	// returns Singleton only if not initialized (so once)
	public static synchronized Singleton getInstance() {
		
		if (s_object == null) {
		    s_object = new Singleton();
		}
		
		return s_object;	
	}     
}
