
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;


public class PredicateStringExampleGeneric {
	
 
	@SuppressWarnings("unchecked")
	private static <T> List<T> allMatches (List<T> paramList, Predicate<String> p) {
		   
		   List<String> returnList = new ArrayList<>();
		   
           for (T posMatch: paramList) {
        	   if (p.test((String) posMatch)) {
        		   returnList.add((String) posMatch);
        	   }
           }
        return (List<T>) returnList;
	}		
		
	public static void main(String[] args) {
		List<String> words = Arrays.asList("bbfbn", "tbdox", "three");
		
		List<String> shortWords = PredicateStringExampleGeneric.allMatches(words, s -> s.length() < 4);
	    List<String> wordsWithB = PredicateStringExampleGeneric.allMatches(words, s -> s.contains("b"));
		List<String> evenLengthWords = PredicateStringExampleGeneric.allMatches(words, s -> (s.length() % 2) == 0);
		
		System.out.println(shortWords);
		System.out.println(wordsWithB);
		System.out.println(evenLengthWords);
	}

}
