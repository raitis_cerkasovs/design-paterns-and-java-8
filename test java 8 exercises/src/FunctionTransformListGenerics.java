import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class FunctionTransformListGenerics {
	
	@SuppressWarnings("unchecked")
	private static <T> List<T> transformedList(List<T> paramList, Function <T, T> f)   {
		   
		   List<String> returnList = new ArrayList<>();
		   
        for (T str: paramList) {
          returnList.add((String) f.apply((T) str));
        }
     return (List<T>) returnList;
	}		
		
	public static void main(String[] args) {
		List<String> words = Arrays.asList("bbfbn", "tbido", "three");
		

        List<String> excitingWords = FunctionTransformListGenerics.transformedList(words, s -> s + "!");
        List<String> eyeWords = FunctionTransformListGenerics.transformedList(words, s -> s.replace("i", "eye"));
         List<String> upperCaseWords = FunctionTransformListGenerics.transformedList(words, String::toUpperCase);
		
		System.out.println(excitingWords);
		System.out.println(eyeWords);
		System.out.println(upperCaseWords);
	}
}