
public class Generics {
	
	public static  <T> T betterElement(String s1, String s2, TwoElementPredicate twoStrPre) {
		
		if (twoStrPre.defMeth(s1, s2) == true) 
			return (T) s1;
		else 
			return (T) s2;
			
	}
	
public interface TwoElementPredicate  {
	Boolean  defMeth(String s1, String s2);	
}
	

	public static void main(String[] args) {
		
		String test1 = "Arta";
		String test2 = "Kruze";
		
		
		
		
		System.out.println(Generics.betterElement(test1, test2, (s1, s2) -> s1.length() > s2.length()));		
		System.out.println(Generics.betterElement(test1, test2, (s1, s2) -> true));
	}

}
