
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;


public class PredicateStringExample {
	
 
	private static List<String> allMatches (List<String> paramList, Predicate<String> p) {
		   
		   List<String> returnList = new ArrayList<>();
		   
           for (String posMatch: paramList) {
        	   if (p.test(posMatch)) {
        		   returnList.add(posMatch);
        	   }
           }
        return returnList;
	}		
		
	public static void main(String[] args) {
		List<String> words = Arrays.asList("bbfbn", "tbdo", "three");
		
		List<String> shortWords = PredicateStringExample.allMatches(words, s -> s.length() < 4);
	    List<String> wordsWithB = PredicateStringExample.allMatches(words, s -> s.contains("b"));
		List<String> evenLengthWords = PredicateStringExample.allMatches(words, s -> (s.length() % 2) == 0);
		
		System.out.println(shortWords);
		System.out.println(wordsWithB);
		System.out.println(evenLengthWords);
	}

}



