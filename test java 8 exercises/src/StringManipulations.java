public class StringManipulations {
	
	public static  String betterString(String s1, String s2, TwoStringPredicate twoStrPre) {
		
		if (twoStrPre.defMeth(s1, s2) == true) 
			return s1;
		else 
			return s2;
			
	}
	
public interface TwoStringPredicate  {
	Boolean  defMeth(String s1, String s2);	
}
	

	public static void main(String[] args) {
		
		String test1 = "Arta";
		String test2 = "Kruze";
		
		
		
		
		System.out.println(StringManipulations .betterString(test1, test2, (s1, s2) -> s1.length() > s2.length()));		
		System.out.println(StringManipulations .betterString(test1, test2, (s1, s2) -> true));
	}

}
