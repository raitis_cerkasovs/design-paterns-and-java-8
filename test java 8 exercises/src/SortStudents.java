import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public class SortStudents {
	
	public static void main(String[] args) {
		
	
	
	Student s1 = new Student("Alan", "Car", 32);
	Student s2 = new Student("Ilja", "Sampras", 17);
	Student s3 = new Student("Ben", "Tie", 34);
	Student s4 = new Student("Shae", "Atlton", 21);
	
	List<Student> myList = Arrays.asList(s1, s2, s3, s4);
	
	Collections.sort(myList, (Student a, Student b) -> { return (a.getAge() - b.getAge()); });		
	System.out.println(myList);
	
	
	Collections.sort(myList, (Student a, Student b) -> { return (b.getAge() - a.getAge()); });		
	System.out.println(myList);
	
	Collections.sort(myList, (Student a, Student b) -> { return (a.getSurname().charAt(0) - b.getSurname().charAt(0)); });		
	System.out.println(myList);
	
	}
	
}

class Student {
	
	private String name;
	private String surname;
	private int age;
	
	
	
	public Student(String name, String surname, int age) {
		this.name = name;
		this.surname = surname;
		this.age = age;
	}
	public String getName() {
		return name;
	}
	public String getSurname() {
		return surname;
	}
	public int getAge() {
		return age;
	}
	
	@Override
	public String toString() {
		return "Student [" + surname + "]";
	}
	
	
}
