import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class FunctionTransformList {
	
	private static List<String> transformedList(List<String> paramList, Function <String, String> f)   {
		   
		   List<String> returnList = new ArrayList<>();
		   
        for (String str: paramList) {
          returnList.add(f.apply(str));
        }
     return returnList;
	}		
		
	public static void main(String[] args) {
		List<String> words = Arrays.asList("bbfbn", "tbido", "three");
		

        List<String> excitingWords = FunctionTransformList.transformedList(words, s -> s + "!");
        List<String> eyeWords = FunctionTransformList.transformedList(words, s -> s.replace("i", "eye"));
         List<String> upperCaseWords = FunctionTransformList.transformedList(words, String::toUpperCase);
		
		System.out.println(excitingWords);
		System.out.println(eyeWords);
		System.out.println(upperCaseWords);
	}
}
