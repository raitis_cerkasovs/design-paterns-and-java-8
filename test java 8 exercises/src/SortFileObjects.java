import java.io.File;



public class SortFileObjects {
	
	
	public static File[] retSubDir(String path) {
		
		File myDir = new File(path); 
		
		
		File[] files  = myDir.listFiles(
			(File f) -> { return f.isDirectory(); }
			);
		
		
		
		return files;
	}
	
	public static void main(String[] args) {
		
		File[] listOfFiles = retSubDir("/Users/raitis/Desktop");
		
		  for (int i = 0; i < listOfFiles.length; i++) 
		  {
		          System.out.println(listOfFiles[i].getName());
		  }
	}

}
