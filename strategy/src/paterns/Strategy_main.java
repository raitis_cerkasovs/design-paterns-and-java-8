package paterns;

public class Strategy_main {

	public static void main(String[] args) {
		Strategy_person p1 = new Strategy_person("walker");
		Strategy_person p2 = new Strategy_person("driver");
		Strategy_person p3 = new Strategy_person("runer");
		
		p1.setI(new Strategy_walking());
		p2.setI(new Strategy_driving());
		p3.setI(new Strategy_run());
		
		System.out.println(p1.getName() + " " + p1.move());
		System.out.println(p2.getName() + " " + p2.move());
		System.out.println(p3.getName() + " " + p3.move());
		
		System.out.println("now:");
		p1.setI(new Strategy_driving());	
		System.out.println(p1.getName() + " " + p1.move());
	}

}