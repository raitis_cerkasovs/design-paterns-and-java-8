package paterns;

public class Strategy_driving implements Strategy_interface {
	@Override
	public String movement() {
		return "drive";
	}
}
