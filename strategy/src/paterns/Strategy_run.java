package paterns;

public class Strategy_run implements Strategy_interface {
	@Override
	public String movement() {
		return "run";
	}
}