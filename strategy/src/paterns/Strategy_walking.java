package paterns;

public class Strategy_walking implements Strategy_interface {
	@Override
	public String movement() {
		return "walk";
	}
}