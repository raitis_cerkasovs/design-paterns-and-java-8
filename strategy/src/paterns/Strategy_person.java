package paterns;

public class Strategy_person {
	private Strategy_interface i;
	private String name;
    // func
	public Strategy_person(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public Strategy_interface getI() {
		return i;
	}
	public void setI(Strategy_interface i) {
		this.i = i;
	}

	public String move() {
		return i.movement();
	}
}
