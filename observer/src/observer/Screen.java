package observer;

import java.util.Observable;
import java.util.Observer;

public class Screen implements Observer {


	@Override
	public void update(Observable o, Object arg) {
		  String resp;
		        if (arg instanceof String) {
		            resp = (String) arg;
		            System.out.println("\nReceived Response: " + resp );
		        }
		        
		        
		    }

}