package observer;

public class runer {

	public static void main(String[] args) {
		
	        System.out.println("Enter Text >");
	 
	        // create an event source - reads from stdin
	        final DataStore eventSource = new DataStore();
	 
	        // create an observer
	        final Screen responseHandler = new Screen();
	 
	        // subscribe the observer to the event source
	        eventSource.addObserver(responseHandler);
	 
	        // starts the event thread
	        Thread thread = new Thread(eventSource);
	        thread.start();
	
	
	}

	
}
