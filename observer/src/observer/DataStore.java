package observer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Observable;

public class DataStore extends Observable implements Runnable
{
//	public String data;
//	
//	public String getData()
//	{return data;}
//	
//	public void setData(String data)
//	{this.data =data;
//		//mark the observable as changed
//		setChanged();}

	@Override
	public void run() {
		try {
            final InputStreamReader isr = new InputStreamReader(System.in);
            final BufferedReader br = new BufferedReader(isr);
            while (true) {
                String response = br.readLine();
                setChanged();
                notifyObservers(response + " accounted");
            }
        }
        catch (IOException e) {
            e.printStackTrace();		
	       }	
	}
}