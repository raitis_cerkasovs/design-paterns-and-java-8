package main;

/**
 * Created by raitis on 31/03/15.
 */
public class ConcreteFactory1 extends AbstractFactory {

    @Override
    AbstractClassAbstractProductA createProductA() {
        return new ProductA1("This is Product A1 from ConcrereteFactory1");
    }

    @Override
    AbstractClassAbstractProductB createProductB() {
        return new ProductB1("This is Product B1 from ConcrereteFactory1");
    }
}
