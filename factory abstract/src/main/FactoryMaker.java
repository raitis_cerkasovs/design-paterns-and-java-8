package main;

/**
 * Created by raitis on 31/03/15.
 */
public class FactoryMaker {

    private static AbstractFactory pf;

    static AbstractFactory getFactory(String choice) {
        if (choice.equals("a"))
            pf = new ConcreteFactory1();
        if (choice.equals("b"))
            pf = new ConcrateFactory2();

        return pf;
    }
}
