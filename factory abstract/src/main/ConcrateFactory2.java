package main;

/**
 * Created by raitis on 31/03/15.
 */
public class ConcrateFactory2 extends AbstractFactory {
    @Override
    AbstractClassAbstractProductA createProductA() {
        return new ProductA2("This is Product A2 from ConcrereteFactory2");
    }

    @Override
    AbstractClassAbstractProductB createProductB() {
        return new ProductB2("This is Product A2 from ConcrereteFactory2");
    }
}
