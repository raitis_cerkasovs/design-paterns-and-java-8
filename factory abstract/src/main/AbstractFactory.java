package main;

/**
 * Created by raitis on 31/03/15.
 */
public abstract class AbstractFactory {
    abstract AbstractClassAbstractProductA createProductA();
    abstract AbstractClassAbstractProductB createProductB();
}
